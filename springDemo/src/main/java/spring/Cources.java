package spring;

public abstract class Cources {
	String courceName;
	String courceNumber;
	String courceHours;
	Student courceDepartment;
	public String getCourceName() {
		return courceName;
	}
	public void setCourceName(String courceName) {
		this.courceName = courceName;
	}
	public String getCourceNumber() {
		return courceNumber;
	}
	public void setCourceNumber(String courceNumber) {
		this.courceNumber = courceNumber;
	}
	public String getCourceHours() {
		return courceHours;
	}
	public void setCourceHours(String courceHours) {
		this.courceHours = courceHours;
	}
	public Student getCourceDepartment() {
		return courceDepartment;
	}
	public void setCourceDepartment(Student courceDepartment) {
		this.courceDepartment = courceDepartment;
	}
	
	public abstract void printCourceInfo();

}
