package spring;
import java.util.ArrayList;
import java.util.GregorianCalendar;
public abstract class Student {
	int id;
	String name;
	int price;
	ArrayList<Primary> primaryCource=new ArrayList<Primary>();
	ArrayList<Elective> electiveCource=new ArrayList<Elective>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	public  abstract void setPrice();
	public  int getPrice()
	{
		return this.price;
	}
	
	public abstract void printInfo();
	public ArrayList<Primary> getPrimaryCource() {
		return primaryCource;
	}
	public void setPrimaryCource(ArrayList<Primary> primaryCource) {
		this.primaryCource = primaryCource;
	}
	public ArrayList<Elective> getElectiveCource() {
		return electiveCource;
	}
	public void setElectiveCource(ArrayList<Elective> electiveCource) {
		this.electiveCource = electiveCource;
	}
	

}
