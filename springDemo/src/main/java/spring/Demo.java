package spring;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.util.ArrayList;


public class Demo {

	public static void main(String[] args) {
		ApplicationContext app =  new ClassPathXmlApplicationContext("AppContext.xml");
		
			ArrayList<ComputerEng>computerSection=new ArrayList<ComputerEng>();
			ArrayList<BioEng>BioSection=new ArrayList<BioEng>();
			ArrayList<Elective>el=new ArrayList<Elective>();
			ArrayList <Primary>pri=new ArrayList<Primary>();
			ComputerEng s1=(ComputerEng) app.getBean("computereng");
			s1.setId(1);
			s1.setName("Ali");
			
			
			
			Elective e = (Elective) app.getBean("elective");
			e.setAvailable(true);
			e.setCourceName("Digital");
			e.setCourceHours("3");
			e.setCourceNumber("332");
			e.setCourceDepartment((ComputerEng) app.getBean("computereng"));
			
			Primary p = new Primary();
			p.setCourceName("AI");
			p.setCourceHours("3");
			p.setCourceNumber("450");
			p.setCourceDepartment((BioEng)app.getBean("bioeng"));
			
			el.add(e);
			pri.add(p);
			
			s1.setElectiveCource(el);
			s1.setPrimaryCource(pri);
			
			s1.printInfo();
			
			
					
	}
}
